use super::pos::{Direction, Pos};
use druid::Data;

use std::{
    iter,
    ops::{Index, Not},
};

#[derive(Clone, Copy, Debug, PartialEq, Eq, Data)]
pub enum Colour {
    Black,
    White,
    Empty,
}
impl Not for Colour {
    type Output = Colour;

    fn not(self) -> Self::Output {
        match self {
            Self::Black => Self::White,
            Self::White => Self::Black,
            Self::Empty => panic!("cannot call not on empty colour"),
        }
    }
}

#[derive(Clone, Copy, Debug, Data)]
pub struct Board {
    black: u64,
    white: u64,
}
impl Index<Pos> for Board {
    type Output = Colour;

    fn index(&self, pos: Pos) -> &Self::Output {
        let index = pos.index();
        if (self.black >> index) & 1 == 1 {
            &Colour::Black
        } else if (self.white >> index) & 1 == 1 {
            &Colour::White
        } else {
            &Colour::Empty
        }
    }
}
impl Default for Board {
    fn default() -> Self {
        Self {
            black: 0x00_00_00_10_08_00_00_00,
            white: 0x00_00_00_08_10_00_00_00,
        }
    }
}
impl Board {
    /// Construct a new instance. Alias for `Board::default()`
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set(&mut self, pos: Pos, colour: Colour) {
        let index = pos.index();
        let mask = 1 << index;
        match colour {
            Colour::Black => {
                self.black |= mask;
                self.white &= !mask;
            }
            Colour::White => {
                self.white |= mask;
                self.black &= !mask;
            }
            Colour::Empty => {
                self.white &= !mask;
                self.black &= !mask;
            }
        }
    }

    pub fn take_all<'a>(
        &'a self,
        pos: &'a mut Pos,
        dir: Direction,
        colour: Colour,
    ) -> impl Iterator<Item = Pos> + 'a {
        iter::from_fn(move || {
            if self[*pos] != colour {
                return None;
            }

            let prev = *pos;

            // Step
            *pos = match pos.step(dir, 1) {
                Some(end) => end,
                None => return None,
            };

            return Some(prev);
        })
    }

    pub fn play(&mut self, pos: Pos, colour: Colour) {
        self.set(pos, colour);
        for (dir, mut end) in pos.neighbours() {
            if self[end] != !colour {
                continue;
            }

            let mut lookahead = end;
            self.take_all(&mut lookahead, dir, !colour).for_each(|_| ());

            if self[lookahead] == colour {
                // These bricks should be turned
                let copy = *self;
                for capture in copy.take_all(&mut end, dir, !colour) {
                    self.set(capture, colour);
                }
            }
        }
    }

    pub fn positions(self, my_colour: Colour) -> Vec<Pos> {
        let mut positions = Vec::new();

        for (start, colour) in self {
            if colour != my_colour {
                continue;
            }

            for (dir, mut end) in start.neighbours() {
                if self[end] != !my_colour {
                    continue;
                }

                // Let's say we're white. This neighbour is black. If we follow this chain, we
                // might find a playable position.

                self.take_all(&mut end, dir, !my_colour).for_each(|_| ());

                if self[end] == Colour::Empty {
                    // We found a playable position
                    positions.push(end);
                }
            }
        }

        positions
    }

    pub fn is_complete(self) -> bool {
        return self.black & self.white == 0xFF_FF_FF_FF_FF_FF_FF_FF;
    }

    pub fn count_scores(self) -> (u8, u8) {
        let mut black = 0;
        let mut white = 0;

        for (_, colour) in self {
            if colour == Colour::Black {
                black += 1;
            } else if colour == Colour::White {
                white += 1;
            } else {
                panic!("an empty square was returned by iterator");
            }
        }

        return (black, white);
    }

    pub fn is_legal(self, colour: Colour, pos: Pos) -> bool {
        self.positions(colour).contains(&pos)
    }
    pub fn has_moves(self, colour: Colour) -> bool {
        !self.positions(colour).is_empty()
    }
}
impl IntoIterator for Board {
    type Item = <BoardIter as Iterator>::Item;
    type IntoIter = BoardIter;

    fn into_iter(self) -> BoardIter {
        BoardIter {
            black: self.black,
            white: self.white,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct BoardIter {
    black: u64,
    white: u64,
}
impl Iterator for BoardIter {
    type Item = (Pos, Colour);

    fn next(&mut self) -> Option<Self::Item> {
        let next_black_index = (64 - self.black.leading_zeros()) as u8;
        let next_white_index = (64 - self.white.leading_zeros()) as u8;

        if next_black_index == 0 && next_white_index == 0 {
            None
        } else if next_white_index > next_black_index {
            let index = next_white_index - 1;
            self.white &= !(1 << index);
            Some((Pos::from_index(index), Colour::White))
        } else {
            let index = next_black_index - 1;
            self.black &= !(1 << index);
            Some((Pos::from_index(index), Colour::Black))
        }
    }
}
