use super::{
    board::{Board, Colour},
    pos::Pos,
};

impl Board {
    fn minimax_inner(
        self,
        max_colour: Colour,
        mut current_colour: Colour,
        depth: u8,
        mut alpha: u8,
        mut beta: u8,
    ) -> (u8, Option<Pos>) {
        let is_complete = self.is_complete();

        // First, check if we should return current scores
        if depth == 0 || is_complete {
            let (black, white) = self.count_scores();

            if is_complete {
                assert_eq!(
                    black + white,
                    64,
                    "game was completed, but not in 64 moves???!!"
                );
            }

            if current_colour == Colour::Black {
                return (black + depth, None);
            } else {
                return (white + depth, None);
            }
        }

        let mut positions = self.positions(current_colour);

        // If the player has no moves, let the other player take another turn.
        if positions.is_empty() {
            current_colour = !current_colour;
            positions = self.positions(current_colour);

            // If both players were out of moves, I assume it's a draw
            if positions.is_empty() {
                return (32, None);
            }
        }

        let mut score = if current_colour == max_colour {
            0
        } else {
            std::u8::MAX
        };
        let mut top_move = None;

        for pos in positions {
            let mut clone = self;
            clone.play(pos, current_colour);
            let (local_score, _) =
                clone.minimax_inner(max_colour, !current_colour, depth - 1, alpha, beta);

            if current_colour == max_colour {
                if local_score > score {
                    score = local_score;
                    top_move = Some(pos);

                    alpha = local_score;
                    if alpha >= beta {
                        // alpha cutoff
                        return (score, top_move);
                    }
                }
            } else {
                if local_score < score {
                    score = local_score;
                    top_move = Some(pos);

                    beta = local_score;
                    if alpha >= beta {
                        // beta cutoff
                        return (score, top_move);
                    }
                }
            }
        }

        (score, top_move)
    }

    pub fn minimax(self, colour: Colour, depth: u8) -> (u8, Option<Pos>) {
        self.minimax_inner(colour, colour, depth, 0, u8::MAX)
    }
}
