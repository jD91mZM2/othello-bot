#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Pos {
    pub x: u8,
    pub y: u8,
}
impl Pos {
    pub fn try_new(x: u8, y: u8) -> Option<Self> {
        if x < 8 && y < 8 {
            Some(Self { x, y })
        } else {
            None
        }
    }
    pub fn new(x: u8, y: u8) -> Self {
        Self::try_new(x, y).expect("tried to create an out-of-bounds position")
    }
    pub fn index(self) -> u8 {
        self.y * 8 + self.x
    }
    pub fn from_index(index: u8) -> Self {
        Self::new(index % 8, index / 8)
    }

    pub fn step(self, dir: Direction, multiplier: u8) -> Option<Self> {
        let (dx, dy) = dir.step(multiplier);
        let x = (self.x as i8).checked_add(dx)? as u8;
        let y = (self.y as i8).checked_add(dy)? as u8;
        Self::try_new(x, y)
    }
    pub fn neighbours(self) -> impl Iterator<Item = (Direction, Self)> {
        Direction::ALL
            .iter()
            .copied()
            .flat_map(move |dir| self.step(dir, 1).map(|pos| (dir, pos)))
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Direction {
    North,
    South,
    West,
    East,

    NorthWest,
    NorthEast,
    SouthWest,
    SouthEast,
}
impl Direction {
    pub const ALL: [Self; 8] = [
        Self::North,
        Self::South,
        Self::West,
        Self::East,
        Self::NorthWest,
        Self::NorthEast,
        Self::SouthWest,
        Self::SouthEast,
    ];
    pub fn offset(self) -> (i8, i8) {
        match self {
            Self::North => (0, -1),
            Self::South => (0, 1),
            Self::West => (-1, 0),
            Self::East => (1, 0),

            Self::NorthWest => (-1, -1),
            Self::NorthEast => (1, -1),
            Self::SouthWest => (-1, 1),
            Self::SouthEast => (1, 1),
        }
    }
    pub fn step(self, multiplier: u8) -> (i8, i8) {
        let (mut dx, mut dy) = self.offset();
        dx *= multiplier as i8;
        dy *= multiplier as i8;
        (dx, dy)
    }
}

#[cfg(test)]
mod tests {
    use super::Pos;

    use quickcheck::{Arbitrary, Gen};
    use quickcheck_macros::quickcheck;

    impl Arbitrary for Pos {
        fn arbitrary(g: &mut Gen) -> Pos {
            Pos {
                x: u8::arbitrary(g) % 8,
                y: u8::arbitrary(g) % 8,
            }
        }
    }

    #[quickcheck]
    fn index(pos: Pos) {
        assert_eq!(pos, Pos::from_index(pos.index()))
    }
}
