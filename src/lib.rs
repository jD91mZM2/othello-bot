use druid::{
    widget::{Button, Checkbox, Flex, Label, Painter},
    AppLauncher, Data, Env, Lens, RenderContext, Widget, WidgetExt, WindowDesc,
};
use wasm_bindgen::prelude::*;

pub mod board;
pub mod draw;
pub mod game;
pub mod minimax;
pub mod pos;

use self::{
    board::Colour,
    game::{Game, MoveState},
    pos::Pos,
};

const DEFAULT_DEPTH: u8 = 8;

#[derive(Clone, Default, Data, Lens)]
pub struct State {
    game: Game,
    autoplay: bool,
}
impl State {
    /// Construct a new instance. Alias for `State::default()`
    pub fn new() -> Self {
        Self::default()
    }

    /// Let player play move
    fn player(&mut self, pos: Pos) {
        if self.game.play_move(pos) == MoveState::Played && self.autoplay {
            self.cpu();
        }
    }

    /// Let cpu play move
    fn cpu(&mut self) {
        self.game.play_cpu(DEFAULT_DEPTH);
    }
}

/// Return a playable GUI widget of the Othello game
pub fn gui_widget() -> impl Widget<State> {
    let mut rows = Flex::column();

    for y in 0..8 {
        let mut columns = Flex::row();
        for x in 0..8 {
            let label = Label::new("")
                .background(Painter::new(move |ctx, state: &State, _env| {
                    let bg = match state.game.board()[Pos::new(x, y)] {
                        Colour::Black => druid::Color::rgb8(0x00, 0x00, 0x00),
                        Colour::White => druid::Color::rgb8(0xFF, 0xFF, 0xFF),
                        Colour::Empty => druid::Color::rgb8(0x00, 0xFF, 0x00),
                    };
                    let rect = ctx.size().to_rect();
                    ctx.fill(rect, &bg);
                }))
                .on_click(move |_ctx, state: &mut State, _env| state.player(Pos::new(x, y)));
            columns.add_flex_child(label.expand(), 1.0);
            columns.add_spacer(1.0);
        }
        rows.add_flex_child(columns, 1.0);
        rows.add_spacer(1.0);
    }

    rows.with_child(
        Button::new("Ask Computer").on_click(move |_ctx, state: &mut State, _env| state.cpu()),
    )
    .with_child(Checkbox::new("Automatically play computer moves").lens(State::autoplay))
    .with_child(Label::new(|state: &State, _env: &Env| {
        format!(
            "Current player is {}",
            if state.game.current_colour() == Colour::Black {
                "black"
            } else {
                "white"
            }
        )
    }))
}

/// Start a playable GUI of this Othello game
pub fn gui_window() -> Result<(), Box<dyn std::error::Error>> {
    let win = WindowDesc::new(self::gui_widget)
        .title("Othello")
        .window_size((1000.0, 1000.0));

    let data = State::new();

    AppLauncher::with_window(win).launch(data)?;
    Ok(())
}

#[wasm_bindgen]
pub fn wasm_main() {
    console_error_panic_hook::set_once();

    gui_window().expect("game initialisation failed");
}
