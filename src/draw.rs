use super::{
    board::{Board, Colour},
    pos::Pos,
};

use std::fmt::{self, Display};

impl Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "   1 2 3 4 5 6 7 8")?;
        for y in 0..8 {
            write!(f, "{} ", y + 1)?;
            for x in 0..8 {
                let pos = Pos::new(x, y);
                write!(
                    f,
                    "{}",
                    match self[pos] {
                        Colour::Black => "○ ",
                        Colour::White => "● ",
                        Colour::Empty => "⛶ ",
                    }
                )?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Display for Pos {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}
