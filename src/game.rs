use super::{
    board::{Board, Colour},
    pos::Pos,
};

use druid::Data;

/// Describes the situation after a move
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MoveState {
    /// Illegal move; nothing was done
    IllegalMove,
    /// Move was legal, and it is now the opponent's turn
    Played,
    /// Move was legal, but the opponent had no legal moves so it's your turn again
    PlayAgain,
    /// Move was legal, and the game is now completed
    Completed,
}

#[derive(Clone, Copy, Data, Debug)]
pub struct Game {
    board: Board,
    current_colour: Colour,
}
impl Default for Game {
    fn default() -> Self {
        Self {
            board: Board::new(),
            current_colour: Colour::Black,
        }
    }
}
impl Game {
    /// Construct a new instance. Alias for `Game::default()`
    pub fn new() -> Self {
        Self::default()
    }

    /// Plays the current move. Returns true if the move was legal and the op
    pub fn play_move(&mut self, pos: Pos) -> MoveState {
        if !self.board.is_legal(self.current_colour, pos) {
            return MoveState::IllegalMove;
        }

        self.board.play(pos, self.current_colour);

        if self.board.is_complete() {
            MoveState::Completed
        } else if !self.board.has_moves(!self.current_colour) {
            MoveState::PlayAgain
        } else {
            self.current_colour = !self.current_colour;
            MoveState::Played
        }
    }

    /// Let the computer play a move. Returns illegal move if it has no legal moves.
    pub fn play_cpu(&mut self, depth: u8) -> MoveState {
        loop {
            let (_score, pos) = self.board.minimax(self.current_colour, depth);

            let pos = match pos {
                None => break MoveState::IllegalMove,
                Some(pos) => pos,
            };

            match self.play_move(pos) {
                MoveState::IllegalMove => panic!("cpu tried illegal move"),
                MoveState::PlayAgain => continue,
                state @ MoveState::Played | state @ MoveState::Completed => break state,
            }
        }
    }

    /// Returns the current colour
    pub fn current_colour(&self) -> Colour {
        self.current_colour
    }

    /// Returns the immutable game board
    pub fn board(&self) -> Board {
        self.board
    }
}
